// Activity on JavaScript Data Types
// sum of numbers
function addNumbers(num1, num2){
	console.log("The sum is: ");
	console.log(num1 + num2);
};

addNumbers(12, 18);


//subtract numbers
function subtNumbers(num3, num4){
	console.log("The difference is: ");
	console.log(num3 - num4);
};

subtNumbers(99, 30);

//multiply numbers
function multNumbers(num5, num6){
	return num5 * num6;
};

let product = multNumbers(5,6);
console.log("The product is:");
console.log(product);

