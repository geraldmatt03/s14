// demo
document.getElementById("btn-1").addEventListener('click', () => {
	alert("Add more!");
});

let paragraph = document.getElementById("paragraph-1");
let paragraph2 = document.getElementById("paragraph-2");

document.getElementById("btn-2").addEventListener('click', () => {
	paragraph.innerHTML = "I can even do this!";
});

document.getElementById("btn-3").addEventListener('click', () => {
	paragraph2.innerHTML = "Or this!";
	paragraph2.style.color = "red";
	paragraph2.style.fontSize = "50px";
});

/*Lesson Proper*/
// Writing comments in JavaScript:
// There are two ways of writing comments in JS:
	// single line comments - ctrl + /
	/*
		multi line comments - ctrl + shift + /

		Comments in JS, much like in CSS and HTML will not be read by the browser. So, these comments are often used to add notes and to add markers to your code
	*/

console.log("Hello World!");

	/*
		JavaScript
			-we can see or log message in our console.

			Consoles are part of our browser which will allow us to see/log or messages, data, or information in our programming language.

			In fact, for most of browser allows us to add some JavaScript expression. Cansoles can be accessed through its developer tools in our console tab.

			Statements
				-statements are simply instructions or expressions we add to our programming language which will then be communicated to our computers.
				-Statements in JavaScript commonly ends in semicolon(;)
				-as a good practice, semicolons are used to end statements even though it doesn't require it.

			Syntax
				-syntax in programming, is a set of rules that describes how statements are properly made or constructed.
				-lines/blocks of code must follow a certain set of rules for it to work. Because remember, you are not merely communicating with another human, in fact you are communicating with a computer.
	*/

console.log("Gerald Matt Maraño");

// Variables
	/*
		In HTML, elements are containers of other elements and text. In JavaScript, variables are containers of data. It is a given name to describe a piece of data.

		Variables also allows us to use or refer to data multiple times.
	*/
// num is the name of the variable
// 10 is the value of the variable
let num = 10;

console.log(6);
console.log(num);

let name1 = "Leonardo";

console.log("John");
console.log(name1);

/*
	Creating Variables
		To create a variable, there are two steps to be done:
			-Declaration - it allows us to create the variable.
			-Initialization - it allows us to add an initial value to a variable.
		Variables in JS are declared with the use of "let" or "const" keyword.
*/

let myVariable;
/*
	var = scoping
		let
		const

	We can create variable without initial value. However, when logged into the console, the variable will return a value of "undefined".

	Undefined
		- is a data type that indicates that the variables exist but there were no values.

	You can always initialize a variable after declaration by assigning a value to the variable with the use of assignment opeartor (=).
*/
console.log(myVariable);

myVariable = "New Initialized Value";
console.log(myVariable);

myVariable = "Another Value";
console.log(myVariable);

myVariable = 7;
console.log(myVariable);
/*
	You cannot and should not access a variable before its been created or declared.
*/


myVariable2 = "Initial value 2";
// let myVariable2;
console.log(myVariable2);

myVariable3 = "hi";
console.log(myVariable3);
/*
	Can you use use or refer to a variable that has not been created or declared?
		-No, this will result in error

	Undefined vs Not Defined
		-Undefined means a variable has been declared but there is no initial value. (it is a data type)
		-Not defined means that the variable you are trying to refer or access does not exist. (it means it is an error)

	NOTE: Some errors in JS will stop the program from further executing.
*/
/*
	let vs const

	with let, we can create variables that can be declared, initialized, and re-assigned.

	In fact we can declare let variables and initialize after.
*/

let bestFinalFantasy;
bestFinalFantasy = "Final Fantasy 6";
console.log(bestFinalFantasy);

// Re-assigning let variables
bestFinalFantasy = "Final Fantasy 7";
console.log(bestFinalFantasy);

/*
	Did the value changed?
	-yes, we can re-assign values to let variables.

	What happens when declaring anothe variable with the same name?
	-it returns an error.

	Can you and should you create variables with the same name?
	-No.
*/

/*let bestFinalFantasy = "Final Fantasy 10";
console.log(bestFinalFantasy);*/

/*const - these are variables with constant data. Therefore we should not re-declare or re-assign a const variable without initialization*/

const pi = 3.1416;
console.log(pi);

const mvp = "Michael Jordan";
console.log(mvp);

mvp1 = "LeBron James";
console.log(mvp1);

mvp1 = "ako";
console.log(mvp1);
/*
	Re-assigning value to a const variable will result in an error.

	const variable are used for data that we expect or do not want its value to change
*/

/*
	Guides on Variable Names:
	1. When naming variables, it is important to create variables that are descriptive and indidcative of the data it contains.
		let firstName = "Michael"; - good variable name
		let pokemon = 25000; - bad variable name

	2. When naming variables, it is better to start with lower case letter. We usually avoid creating variable names that starts with capital letters because there are several keywords in JS that starts in a capital letter.
		let firstName = "Juan"; - good variable name
		let FirstName = "Juan"; - bad variable name

	3. Do not add spaces to your variable names. Use camelCase for multiple words or underscore (_).
		let firstName = "Juan";
		let _start - class

		RegExp - reserved word for JS
		break
		goto
*/

let numSum = 5000;
let num_sum= 6000;
console.log(numSum);
console.log(num_sum);

// Declare multiple variables
let brand = "Toyota", model = "Vios"; type = "Sedan";
console.log(brand);
console.log(model);
console.log(type);

// console logging for more variables: use comma to separate each variable
console.log(brand, model, type);

/*
	Data Types
	Number Data type - there are number data which can be used for mathematical operations.
		-integers (whole numbers)
		- float (decimal numbers)
*/

let numString1 = "5";
let numString2 = "6";
let num1 = 5;
let num2 = 6;
console.log(numString1 + numString2); //56 strings are concatinated
console.log(num1 + num2); //11 both operands, both argument in the operation are numbers.
let num3 = 5.5;
let num4 = .5;
console.log(num1 + num3); //10.5
console.log(num3 + num4); // 6

/*
	When the + operator is used on numbers, it will do proper mathematical operations. However when used on strings, it will concatenate.

	forced coercion
		- when the one's data type is forced to change to complete an operation.

		ex. string + num = concatenation
*/

console.log(numString1 + num1); //55
// parseInt() - this can change the type of a numeric string to a proper number

console.log(num4 + parseInt(numString1));

let sum1 = num1 + parseInt(numString2);
console.log(sum1); //11 - numString2 was properly converted into a number

// Mathematical Operators (-, *, /, %)
// Subtraction
	console.log(num1 - num3); //-0.5 results in proper mathematical operation
	console.log(num3 - num4); //5 results in proper mathematical operation
	console.log(numString1 - num2); //-1 results in propero mathematical operation, in subtraction, numeric string will not concatenate and instead will be forcibly change the type and subtract it properly

	let sample = "Thonie"
	console.log(sample - numString2); //NaN result is not a number, when trying to perform between the alphanumeric string and numeric string, the result is NaN.

//Multiplication
	console.log(num1 * num2); //30
	console.log(numString1 * num1); //25
	console.log(numString1 * numString2); //30

	let product = num1 * num2;
	let product2 = numString1 * num1;
	let product3 = numString1 * numString2;

//Division
	console.log(product / num2); //5
	console.log(product2); //25
	console.log(product2 / 5); //5
	console.log(numString2 / numString1); //1.2

// note Division and multiplication by 0
	console.log(product2 * 0); // 0
	console.log(product3 / 0); //Infinity
	// division by 0 is not accurately and should not be done as it will result into infinity.

// % modulo - remainder of a division operation
console.log(product2 % num2); // remainder - 1
console.log(product2 % num1); // remainder - 0

// Boolean (true or false / 1 or 0)
/*
	Boolean is usually used for logic operations for if-else conditions.
	When creating a variable which will contain boolean, the variable name is usually a yes or no question
*/
let isAdmin = true;
let isMarried = false;
let isMVP = true;
let isfinancialAdviser = true;

// you can concatenate string and boolean
console.log("Is she married?" + isMarried);
console.log("Is Jimmu married?" + isMarried);
console.log("Is Mon the MVP?" + isMVP);
console.log("Is Clifford the current admin?" + isAdmin);
console.log("Is Nica the best financial adviser?" + isfinancialAdviser);

// Arrays
	/*
		Arrays are a special kind of data type that store multiple values. Arrays can actually store data with different types BUT as the best practice, arrays are used to contain multiple values with the same types of data.

		Values in an array are separated by commas(,).
		an array is created wuth an Array literal = []

		Array are better thought as groups of data. 
	*/
let array1 = ["Goku", "Piccolo", "Gohan", "Vegetta"];
console.log(array1);
console.log(array1[0]);

let array2 = ["One Punch Man", true, 500, "Saitama"];
console.log(array2);

// Objects
/*
	Objects are another special kind of data type used to mimic the real world.
		-used to create complex data that contain pieces of information that is relevant to each other.
		-object are created with object literals = {}
		-each data/value are paired with a key
		-each field is called property
		-each field is separated by commas (,)
	mobilePhone = 
				color: "red"
				model: "2022"
				brand: iPhone
*/
let hero = {
	heroName: "One Punch Man",
	isActive: true,
	salary: 500,
	realName: "Saitama",
	height: 200
};
console.log(hero.height);

/*
	Mini-Activity

	Create a variable with a group of data
		-the group of data should contain names from your favorite band.

	Create a variable which contain multiple values of differing types and describes a single person
		-this data type should be able to contain multiple key value pairs:
			firstName: <Value>
			lastName: <Value>
			isDeveloper: <Value>
			hasPortfolio: <Value>
			age: <Value>
*/

// band with members
let queen = ["Freddie Mercury", "Brian May", "John Deacon", "Roger Taylor"];
console.log(queen);

// second part of mini-activity
let developerProfile = {
	firstName : "Gerald Matt",
	lastName : "Maraño",
	isDeveloper : true,
	hasPortfolio: true,
	age: 22
};
console.log(developerProfile);

// In strings, spaces count as characters
let state = "Texas";
let counrty = "USA";
let address = state + ',' + ' ' + counrty;
console.log(address);

// Null vs Undefined
/*
	Null is the explicit absence of data or value. This is done to show that a variable contains nothing as opposed to undefined which means that the variable is created but there is no initial value.
*/
// use cases of Null
	// when doing a query or search, there of course might be a zero result.
	let foundResult = null;
	console.log(foundResult);

// undefined - is a representation that a variable has been created or declared but there is no initial value, so we can't quite say what the value is, thus it is undefined
let sampleVariable;
console.log(sampleVariable);

let person1 = {
	name: "Peter",
	age: 42
}
console.log(person1.isAdmin);
//  we use dot notation to select or display the values of property of an object
/*
	For undefined, this is normally caused by developers when creating variables that have no data/value associated/initialiezd with them.

	undefined, because person1 variable does exist, however, there is no property in the object called isAdmin.
*/

/*
	Functions
		-function in JS, are lines or blocks of codes that tell our device/application to perform certain task when called or invoked.

		-function are reusable pieces of code with instructions which can be used over and over again just so long as we can call or invoke it.
*/

let name2 = "Bruno";
console.log(name2);
console.log(name2);
console.log(name2);

// functions are created by declaring the function using the "function" keyword
function showLeo(){
	console.log("Leo");
	console.log("Leo");
	console.log("Leo");
};

// function invocation/call is the term where the function is used
showLeo();
showLeo();
/*
	Function declaration is when the function is created.
	Function invocation is when the function is used.
*/

/*
	Arguments and Parameters

	-declared a function using "function" keyword
	-name - parameters are representation of the argument from an invocation

	-we can use the paramter within the function

	-we can also pass data into the function through our invocation.
	-Data added into the parenthesis of a funtion invocation will be or can be passed into our funtion, thi isi called argument.
*/
 function greet(name){
 	// console.log("name");
 	console.log("Hello!" + name + "," + " " + "how are you doing?");
 };

 greet("Jake");
 greet("Pat");

 /*
	Mini-Activity
	Create a function which is able to display data by passing an argument. The data will be displayed in the console.
		Data is fun!
		JavaScript is fun!
		Reading is fun!

		Name you function as displayMsg()
 */
 function displayMsg(data1){
 	console.log(data1 + " " + "is fun!");
 };

 displayMsg("Data");
 displayMsg("JavaScript");
 displayMsg("Reading");

 /* Multiple Arguments
	A function can also receive multiple arguments and therefore, to be able to use and access these arguements we're also going to need multiple paramters.
 */

 function displayFullName(firstName1, lastName1, age){
 	console.log(firstName1 + " " + lastName1 + " ");
 	console.log("You are "+ age + " years old");
 };
// the number of arguements should match the number of the parameters
 displayFullName("Jeff", "Bezos", 25);
// NOTE: order matters in your argument and parameters
 displayFullName("Cena", "John", 32);
 displayFullName(32, "John", "Cena");

 function showSum(n1,n2){
 	console.log("The sum is: ");
 	console.log(n1 + n2);
 };

 showSum(15,10);

 // you can pass variables as arguments
 let sampleNumber1 = 30;
 let sampleNumber2 = 25;

 showSum(sampleNumber1, sampleNumber2);

 /*
	Variable Scopes
		Variables and constant have scopes.
			- meaning if a variable or constant is declared outside of a function, any function succeeding it will be able to have access to that variable.

			However, any variable or constant declared inside a function can only be used within that function or code block.

			Function scoped variables and constants can only be declared
 */

 let sample1 = "This is a sample.";
 const sampleConst = "Sample constant";

 function sampleFunction(){
 	console.log(sample1);
 	console.log(sampleConst);
 };

 sampleFunction();

 function sampleFunction2(){
 	console.log(sample1);
 };
 sampleFunction2();

 function sampleFunction3(){
 	let sample2 = "This is inside a function";
 	const sampleConst2 = "This is in a function";

 	console.log(sample2);
 	console.log(sampleConst2);
 };

 /*sampleFunction3();

 function sampleFunction4(){
 	console.log(sampleConst2);
 };
sampleFunction4();
*/

/*
	Return keyword allows to return a value.
	This also stops the process of the function and any statement after the return should not or will not be processed.
*/

function addNum(x,y){
	return x + y;
};

// with the return keyword you can return a value and save that value in a variable
let sampleSum = addNum(2,3);
console.log(sampleSum);

let sampleSum1 = showSum(5, 10);
console.log(sampleSum1);

function returnFullName(firstName, middleName, lastName){
	return firstName + middleName + lastName;
}; 

let fullName = returnFullName("Michael", "John", "Smith");
console.log("My full name is " + fullName);

function completeDetails(fullName, age, role){
	return{
		fullName: fullName,
		age: age,
		role: role
	};
	// you can return any data type (string, number, boolean, object, array)
	// you can also return a variable created inside a function
};

let userName = completeDetails(fullName, 35, "Developer");
console.log(userName);